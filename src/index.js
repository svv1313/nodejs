const express = require('express');
const app = express();
const fs = require('fs');
app.post('/touch', (req, res) => {
    let fileName = 'noName.txt';
    if(Object.keys(req.query).length !== 0) fileName = req.query.filename;

    fs.writeFile(fileName, JSON.stringify('F**k you'), (err) => {
        if (err) throw err;
        return res.status(200).send({fileName})
    })
});
app.put('/echo', (req, res) => {
    if(Object.keys(req.query).length === 0) return res.status(404).send('404 Not Found');
    let { line, filename, truncate} = req.query;
    let mod = truncate? 'appendFile' : 'writeFile';
    fs[mod](filename, JSON.stringify(line), (err) => {
        if (err) throw err;
        return res.status(200).send({filename, line})
    });
});
app.delete('/rm', (req, res) => {
    if(Object.keys(req.query).length === 0) return res.status(404).send('404 Not Found');
    fs.unlink(req.query.filename, function (err) {
        if (err) throw err;
        return res.status(200).send('susses');
    });
});
app.put('/cp', (req, res) => {
    if(Object.keys(req.query).length === 0) return res.status(404).send('404 Not Found');
    let { from, to } = req.query;
    fs.readFile(from, (err, data) => {
        if (err) throw err;
        let fileContent = JSON.parse(data);
        fs.writeFile(to, JSON.stringify(fileContent), (err) => {
            if(err) throw err;
            return res.status(200).send({ from, to, fileContent})
        })
    });
});
app.put('/mv', (req, res) => {
    if(Object.keys(req.query).length === 0) return res.status(404).send('404 Not Found');
    let { from, to } = req.query;
    fs.readFile(from, (err, data) => {
        if (err) throw err;
        let fileContent = JSON.parse(data);
        fs.writeFile(to, JSON.stringify(fileContent), (err) => {
            if(err) throw err;
            fs.unlink(from, (err, data) => {
                if(err) throw err;
                return true
            });
            return res.status(200).send({ from, to, fileContent})
        })
    })
});
app.listen(3001);
